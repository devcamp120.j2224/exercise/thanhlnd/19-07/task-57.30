package com.example.demo.Model;

import java.util.ArrayList;


public class Student extends Person {

    private int student ;
    private ArrayList<Subject> listSubject ;


    public int getStudent() {
        return student;
    }

    public void setStudent(int student) {
        this.student = student;
    }

    public ArrayList<Subject> getListSubject() {
        return listSubject;
    }

    public void setListSubject(ArrayList<Subject> listSubject) {
        this.listSubject = listSubject;
    }

    public Student() {
        
    }

    public Student(int student, ArrayList<Subject> listSubject) {
        this.student = student;
        this.listSubject = listSubject;
    }

    public Student(int age, String gender, String name, Address address, int student, ArrayList<Subject> listSubject) {
        super(age, gender, name, address);
        this.student = student;
        this.listSubject = listSubject;
    }

    public Student(int age, String gender, String name, Address address, int student, ArrayList<Subject> listSubject , ArrayList<Animal>listPet) {
        super(age, gender, name, address , listPet);
        this.student = student;
        this.listSubject = listSubject;
    }


    @Override
    public void eat() {
        System.out.println("Student eating");
        
    }
    
    public void doHomework(){
        System.out.println("Student do homework");
    }
}
